#!/usr/bin/env bash
  ffmpeg -video_size 1366x768 \
          -framerate 50 \
          -f x11grab \
          -i :0 \
          -f alsa \
          -i default \
          -f webm \
          -cluster_size_limit 20M \
          -cluster_time_limit 5100 \
          -content_type video/webm \
          -c:a libvorbis \
          -b:a 96K \
          -c:v libvpx \
          -b:v 1.5M \
          -crf 30 \
          -g 150 \
          -deadline good \
          -threads 2 \
          grabacion.webm
